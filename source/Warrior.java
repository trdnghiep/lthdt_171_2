public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
	}

	@Override
	public double getCombatScore() {
		if(Utility.isPrime(Battle.GROUND) == true && Battle.GROUND != 4) return getBaseHp()*2;
		return (getWp() == 1)?getBaseHp():getBaseHp()/(double)10;
	}
    
}
