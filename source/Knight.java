public class Knight extends Fighter {

	public Knight(int baseHp, int wp) {
		super(baseHp, wp);
	}
	@Override
	public double getCombatScore() {
		if(Utility.isSquare(Battle.GROUND) == true && Battle.GROUND >= 0) return getBaseHp()*2;
		return (getWp() == 1)? getBaseHp() : getBaseHp()/(double)10;
	}
}
