public class Paladin extends Knight {
	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
	}

	public double getCombatScore() {
		if(getFibonacci(Battle.GROUND) > 2) return (1000 + getFibonacci(Battle.GROUND));
		return 3*getBaseHp();
	}
	public int getFibonacci(int Fibo) {
		int Fi1 = 1;
		int Fi2 = 1;
		int FiTemp;
		int n = 2;
		while(Fi2 < Fibo) {
			FiTemp = Fi1;
			Fi1 = Fi2;
			Fi2 += FiTemp;
			n++;
			if(Fi2 == Fibo) return n;
		}
		return -1;
	}
	
}
